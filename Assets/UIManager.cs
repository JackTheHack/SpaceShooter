﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public PlayerShipManager shipManager; // the data source.

    public Image ShieldUI;
    public Image ArmourUIUp;
    public Image ArmourUIDown;
    public Image ArmourUILeft;
    public Image ArmourUIRight;
    public Image HealthUI;

    Text shieldText;
    Text healthText;

	// Use this for initialization
	void Start () {

        shieldText = ShieldUI.GetComponentInChildren<Text>();
        healthText = HealthUI.GetComponentInChildren<Text>();
    }

	// Update is called once per frame
	void Update () {
        //t = Mathf.PingPong(Time.time, 1);
        bool shipPresent = shipManager.currentlyPiloted != null;

        float currentShield = shipPresent ? shipManager.currentlyPiloted.shield.current : 0f;
        float normalisedShield = currentShield / (shipPresent ? shipManager.currentlyPiloted.shield.max : 0f);

        //Debug.Log(currentShield);
        ShieldUI.fillAmount = normalisedShield;
        shieldText.text = currentShield.ToString();

        for (int i = 0; i < 4; i++)
        {
            
            
            float currentArmour = shipPresent ? shipManager.currentlyPiloted.armourQuadrants[i].current : 0f;
            float normalisedArmour = currentArmour / (shipPresent ? shipManager.currentlyPiloted.armourQuadrants[i].max : 0f);

            if (shipPresent)
            {
                if (shipManager.currentlyPiloted.armourQuadrants[i].direction == new Vector2(0, 1))
                {
                    ArmourUIUp.fillAmount = normalisedArmour;
                }
                if (shipManager.currentlyPiloted.armourQuadrants[i].direction == new Vector2(0, -1))
                {
                    ArmourUIDown.fillAmount = normalisedArmour;
                }
                if (shipManager.currentlyPiloted.armourQuadrants[i].direction == new Vector2(-1, 0))
                {
                    ArmourUILeft.fillAmount = normalisedArmour;
                }
                if (shipManager.currentlyPiloted.armourQuadrants[i].direction == new Vector2(1, 0))
                {
                    ArmourUIRight.fillAmount = normalisedArmour;
                }
            }
            else
            {
                ArmourUIUp.fillAmount = normalisedArmour;
                ArmourUIDown.fillAmount = normalisedArmour;
                ArmourUILeft.fillAmount = normalisedArmour;
                ArmourUIRight.fillAmount = normalisedArmour;
            }
        }

        float currentHealth = shipPresent ? shipManager.currentlyPiloted.health.current : 0f;
        float normalisedHealth = currentHealth / (shipPresent ? shipManager.currentlyPiloted.health.max : 0f);

        HealthUI.fillAmount = normalisedHealth;
        healthText.text = currentHealth.ToString();
    }
}
