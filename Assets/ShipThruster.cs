﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipThruster : MonoBehaviour {

    Vector2 direction; // local direction

    float EngineThrustPower = 1f;

    Rigidbody2D parentShipRigidbody;

	// Use this for initialization
	void Start () {
        parentShipRigidbody = GetComponentInParent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Setup(Vector2 direction, float thrust)
    {
        this.direction = direction;
        this.EngineThrustPower = thrust;
    }

    public Vector2 ApplyAvailableThrust(Vector2 requestedThrustDirection)
    {
        // If the player requests forward thrust, this thruster will attempt to give
        // any component of its thrust in the forward direction.

        Vector2 requestedDirectionRelativeToShip = transform.TransformDirection(requestedThrustDirection);

        float requestedComponent = Vector2.Dot(requestedDirectionRelativeToShip, transform.TransformDirection(direction));
        if (requestedComponent <= 0)
        {
            return Vector2.zero;
        }

        Vector2 result = (requestedComponent * requestedDirectionRelativeToShip) * EngineThrustPower;

        Debug.DrawRay(transform.position, requestedDirectionRelativeToShip * 2);
        Debug.DrawRay(transform.position, result * 2, Color.red);

        return result;
    }
}
