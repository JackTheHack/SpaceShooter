﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class ShipGeneratorEditor : EditorWindow
{

    Color foregroundColor = Color.red;
    Color backgroundColor = Color.clear;
    bool useShade;
    bool useRandomColor;

    string savePath;

    Texture2D currentShipTexture;
    Texture2D workingMaskTexture;
    Color[] workingMaskTexturePixels = new Color[8 * 16];

    Vector2 maskCanvasLocation = new Vector2(30, 200);
    Vector2 maskCanvasSize = new Vector2(65, 130);

    int selectedMaskColour = 0;
    string[] maskColourSelectionTexts = new string[] { "0", "1", "2", "3", "4", "5" };

    [MenuItem("GameObject/Ship Generator Wizard")]
    static void Init()
    {
        ShipGeneratorEditor window = (ShipGeneratorEditor)EditorWindow.GetWindow(typeof(ShipGeneratorEditor));
        window.minSize = new Vector2(200, 300);
        window.Show();
    }

    private void OnEnable()
    {

        workingMaskTexture = new Texture2D(8, 16);
        workingMaskTexturePixels = new Color[8 * 16];
        for (int i = 0; i < workingMaskTexturePixels.Length; i++)
        {
            workingMaskTexturePixels[i] = Color.black;
        }

        workingMaskTexture.SetPixels(workingMaskTexturePixels);
        workingMaskTexture.Apply();
        workingMaskTexture.filterMode = FilterMode.Point;


        currentShipTexture = GenerateShipTexture();
    }

    private void OnGUI()
    {
        GUILayout.Label("Settings", EditorStyles.boldLabel);

        if (GUILayout.Button("Generate Ship"))
        {
            currentShipTexture = GenerateShipTexture();
        }

        useShade = EditorGUILayout.Toggle("Use Shade", useShade);
        useRandomColor = EditorGUILayout.Toggle("Use Random Color", useRandomColor);
        foregroundColor = EditorGUILayout.ColorField("Foreground Color", foregroundColor);
        backgroundColor = EditorGUILayout.ColorField("Background Color", backgroundColor);

        EditorGUI.DrawTextureTransparent(new Rect(maskCanvasLocation.x, maskCanvasLocation.y, maskCanvasSize.x, maskCanvasSize.y), workingMaskTexture);
        selectedMaskColour = GUILayout.SelectionGrid(selectedMaskColour, maskColourSelectionTexts, 6, "toggle");
        if (GUILayout.Button("Reset Mask Canvas"))
        {
            workingMaskTexture.SetPixels(workingMaskTexturePixels);
            workingMaskTexture.Apply();
        }


        if (currentShipTexture != null)
        {
            Color guiColor = GUI.color;
            GUI.color = Color.clear;
            EditorGUI.DrawTextureTransparent(new Rect(170, 200, 130, 130), currentShipTexture);
            GUI.color = guiColor;
        }

        savePath = EditorGUILayout.TextField("File Path", savePath);
        if (GUILayout.Button("Save"))
        {
            SaveTextureAsAsset();
        }

        if (Event.current.type == EventType.MouseDown || Event.current.type == EventType.MouseDrag)
        {
            bool result = TryDrawOnMaskCanvas(Event.current.mousePosition);
            if (result)
            {
                Repaint();
            }
        }

    }

    bool TryDrawOnMaskCanvas(Vector2 mousePos)
    {
        bool success = false;
        float pixelWidth = maskCanvasSize.x / workingMaskTexture.width;
        float pixelHeight = maskCanvasSize.y / workingMaskTexture.height;
        if (mousePos.x > maskCanvasLocation.x && mousePos.x < maskCanvasLocation.x + maskCanvasSize.x)
        {
            if (mousePos.y > maskCanvasLocation.y && mousePos.y < maskCanvasLocation.y + maskCanvasSize.y)
            {
                int x = Mathf.RoundToInt((mousePos.x - maskCanvasLocation.x - (pixelWidth / 2)) / pixelWidth);
                int y = Mathf.RoundToInt(-(mousePos.y - maskCanvasLocation.y + (pixelHeight / 2)) / pixelHeight);
                workingMaskTexture.SetPixel(x, y, ColorFromMaskSelection(selectedMaskColour));
                success = true;
            }
        }
        workingMaskTexture.Apply();
        return success;
    }

    private Texture2D GenerateShipTexture()
    {
        int[] mask = MaskTextureToMask(workingMaskTexture);
        //Sprite spritePreview = GenerateSprite((int[])RandomPlayerGenerator.PlayerMask.Clone());
        Sprite spritePreview = GenerateSprite(mask);
        return spritePreview.texture;
    }

    public Sprite GenerateSprite(int[] mask)
    {
        int maskWidth = 8;
        int maskHeight = 16;

        if (useRandomColor)
        {
            foregroundColor = SpriteGenerator.GetRandomHSVColour(0f, 1f, 0.6f, 1f, 0.5f, 1f);
        }

        return SpriteGenerator.GetSymmetricalMaskedSprite(maskWidth, maskHeight, mask, foregroundColor, backgroundColor, useShade);
    }

    Color ColorFromMaskSelection(int maskSelection)
    {
        if (maskSelection == 0)
        {
            return Color.black;
        }
        else if (maskSelection == 1)
        {
            return Color.magenta;
        }
        else if (maskSelection == 2)
        {
            return Color.white;
        }
        else if (maskSelection == 3)
        {
            return Color.green;
        }
        else if (maskSelection == 4)
        {
            return Color.cyan;
        }
        else if (maskSelection == 5)
        {
            return Color.red;
        }
        return Color.black;
    }

    int[] MaskTextureToMask(Texture2D maskTexture)
    {
        Color[] pixels = maskTexture.GetPixels();
        int[] mask = new int[pixels.Length];
        for (int i = 0; i < pixels.Length; i++)
        {
            if (pixels[i] == Color.magenta)
            {
                mask[i] = 1;
            }
            else if (pixels[i] == Color.white)
            {
                mask[i] = 2;
            }
            else if (pixels[i] == Color.green)
            {
                mask[i] = 3;
            }
            else if (pixels[i] == Color.cyan)
            {
                mask[i] = 4;
            }
            else if (pixels[i] == Color.red)
            {
                mask[i] = 5;
            }
            else
            {
                mask[i] = 0;
            }
        }
        return mask;
    }

    public void SaveTextureAsAsset()
    {
        byte[] bytesToWrite = currentShipTexture.EncodeToPNG();
        FileStream stream = new FileStream("Assets/" + savePath + ".png", FileMode.OpenOrCreate, FileAccess.Write);
        BinaryWriter writer = new BinaryWriter(stream);
        for (int i = 0; i < bytesToWrite.Length; i++)
        {
            writer.Write(bytesToWrite[i]);
        }
        writer.Close();
        stream.Close();

        AssetDatabase.ImportAsset("Assets/" + savePath + ".png", ImportAssetOptions.ForceUpdate);

        // Set to point filter since otherwise they will look terrible when scaled up due to their small size.
        Texture2D tex = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/" + savePath + ".png");
        tex.filterMode = FilterMode.Point;
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }
}
