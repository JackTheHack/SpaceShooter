﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum ShipSize { TINY, SMALL, MEDIUM, LARGE, HUGE }

public class Ship : MonoBehaviour {

    public ShipThruster thrusterPrefab;
    public WeaponMono shipWeaponPrefab;

    WeaponGenerator weaponGen;

    ShipSize size;
    public float MovementSpeed = 1f;
    public float TurnSpeed = 3f;

    public class Health {

        public float max;
        public float current;
        public float repairRate;

        public Health(float max, float repairRate)
        {
            this.max = max;
            this.current = max;
            this.repairRate = repairRate;
        }

        public void Repair()
        {
            current += repairRate;
            if (current > max)
            {
                current = max;
            }
        }

        public void SetCurrent(float amount)
        {
            current = amount;
            if (current > max)
            {
                current = max;
            }
            if (current < 0)
            {
                current = 0;
            }
        }

    }

    public class Shield {

        public float max;
        public float current;
        public float repairRate;
        public float chargeRate;
        public float rechargePercentage; // Charge percentage after having been fully discharged.

        public Shield(float max, float repairRate, float chargeRate)
        {
            this.max = max;
            this.current = max;
            this.repairRate = repairRate;
            this.chargeRate = chargeRate;
            this.rechargePercentage = 0;
        }

        public void Repair()
        {

            if (current == 0)
            {
                Recharge();
                return;
            }

            current += repairRate;
            if (current > max)
            {
                current = max;
            }
        }

        private void Recharge()
        {
            rechargePercentage += chargeRate;
            if (rechargePercentage >= 100)
            {
                current = max / 10f; // Start off on 10% of total shield.
                rechargePercentage = 0;
            }
        }

        public void ResetRecharge()
        {
            rechargePercentage = 0;
        }

        public void SetCurrent(float amount)
        {
            current = amount;
            
            if (current > max)
            {
                current = max;
            }
            if (current < 0)
            {
                current = 0;
            }
        }

        public void Remove(float amount)
        {
            current -= amount;

            if (current > max)
            {
                current = max;
            }
            if (current < 0)
            {
                current = 0;
            }
        }
    }

    public class Armour {

        public float max;
        public float current;
        public float repairRate;
        public Vector2 direction;

        public Armour(float max, float repairRate, Vector2 direction)
        {
            this.max = max;
            this.current = max;
            this.repairRate = repairRate;
            this.direction = direction;
        }

        public void Repair()
        {
            current += repairRate;
            if (current > max)
            {
                current = max;
            }
        }

        public void SetCurrent(float amount)
        {
            current = amount;
            if (current > max)
            {
                current = max;
            }
            if (current < 0)
            {
                current = 0;
            }
        }
    }

    public Health health { get; private set; }
    public Shield shield { get; private set; }
    public Armour[] armourQuadrants { get; private set; }

    bool IsSetup = false;

    public Tilemap tilemap { get; private set; }
    //ThrusterPosition[] thrusterPositions;
    public ShipThruster[] shipThrusters { get; private set; }
    public WeaponMono[] weaponSlots { get; private set; }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (IsSetup)
        {
            shield.Repair();
            for (int i = 0; i < armourQuadrants.Length; i++)
            {
                armourQuadrants[i].Repair();
            }
            health.Repair();
        }
    }

    ContactPoint2D[] hitBuf = new ContactPoint2D[1];
    private void OnCollisionEnter2D(Collision2D collision)
    {
        collision.GetContacts(hitBuf);

        Vector2 normal = hitBuf[0].normal * -1;

        float damage = collision.gameObject.GetComponent<ProjectileController>().parentWeapon.baseDamage;

        DamageController.DamageAmountAndDirection damageStruct = new DamageController.DamageAmountAndDirection(damage, normal);
        Debug.DrawRay(hitBuf[0].point, -hitBuf[0].normal, Color.white, 5);
        Damage(damageStruct);
        //Debug.Log("Bullet did " + damage + " damage to " + gameObject);
    }

    void Damage(DamageController.DamageAmountAndDirection damageStruct)
    {
        if (shield.current > 0)
        {
            shield.Remove(damageStruct.amount);
            Debug.Log(shield.current);
            if (shield.current <= 0)
            {
                shield.SetCurrent(0);
            }
            Debug.Log("Shield hit for " + damageStruct.amount + " damage.");
            return;
        }
        else
        {
            string direction = string.Empty;

            float shipAngle = transform.eulerAngles.z;

            Vector2 unRotatedNormal = Quaternion.Euler(0, 0, -shipAngle) * damageStruct.direction;
            Debug.Log(unRotatedNormal);
            unRotatedNormal.x = Mathf.Round(unRotatedNormal.x);
            unRotatedNormal.y = Mathf.Round(unRotatedNormal.y);
            
            for (int i = 0; i < armourQuadrants.Length; i++)
            {
                if (armourQuadrants[i].current >= 0 && armourQuadrants[i].direction == unRotatedNormal) // might need to do some rounding to be totally accurate.
                {
                    
                    armourQuadrants[i].current -= damageStruct.amount;
                    if (armourQuadrants[i].current <= 0)
                    {
                        armourQuadrants[i].current = 0;
                    }
                    direction = DirectionToString(armourQuadrants[i].direction);
                    Debug.Log("Armour hit for " + damageStruct.amount + " damage on " + direction + " side.");
                    shield.ResetRecharge();
                    return;
                }
            }

        }

        if (health.current > 0)
        {
            health.SetCurrent(health.current - damageStruct.amount);
            shield.ResetRecharge();
            if (health.current <= 0)
            {
                health.SetCurrent(0);
                Destroy(gameObject);
            }
            Debug.Log("Health hit for " + damageStruct.amount + " damage");
            return;
        }
    }

    public static string DirectionToString(Vector2 direction)
    {
        if (direction == Vector2.up)
        {
            return "UP";
        }
        else if (direction == Vector2.down)
        {
            return "DOWN";
        }
        else if (direction == Vector2.right)
        {
            return "RIGHT";
        }
        else if (direction == Vector2.left)
        {
            return "LEFT";
        }
        else
        {
            return "INCONCLUSIVE";
        }
    }

    //public void Setup(ShipSize size, Sprite s, float movementSpeed, float turnSpeed, bool isPlayer)
    //{

    //    health = new Health(100, 0.01f);
    //    shield = new Shield(100, 0.01f, 0.1f);
    //    armourQuadrants = new Armour[4];
    //    Vector2[] directions = new Vector2[] { Vector2.up, Vector2.right, Vector2.down, Vector2.left };
    //    for (int i = 0; i < armourQuadrants.Length; i++)
    //    {
    //        armourQuadrants[i] = new Armour(100, 0.01f, directions[i]);
    //    }

    //    weaponGen = new WeaponGenerator();
    //    this.size = size;
    //    this.MovementSpeed = movementSpeed;
    //    this.TurnSpeed = turnSpeed;

    //    // These two lines are for sprite ships not tilemap ships.
    //    GetComponent<SpriteRenderer>().sprite = s;

    //    GetComponent<BoxCollider2D>().size = GetComponent<Renderer>().bounds.size;

    //    CreateShipThrusters();
    //    CreateWeaponSlots();
    //    FillWeaponSlotsWithRandomWeapons(isPlayer);
    //    IsSetup = true;
    //}

    public void Setup(ShipSize size, ShipTilemap t, float movementSpeed, float turnSpeed, bool isPlayer)
    {
        tilemap = GetComponent<Tilemap>();
        
        weaponGen = new WeaponGenerator();
        this.size = size;
        this.MovementSpeed = movementSpeed;
        this.TurnSpeed = turnSpeed;

        // These two lines are for sprite ships not tilemap ships.
        //GetComponent<SpriteRenderer>().sprite = s;

        //GetComponent<Collider2D>().size = GetComponent<Renderer>().bounds.size;

        CreateTilemapShipComponents(t, isPlayer);
        //CreateWeaponSlots();
        //FillWeaponSlotsWithRandomWeapons(isPlayer);

        // Set centre of mass to middle of origin ship tile.
        GetComponent<Rigidbody2D>().centerOfMass = tilemap.cellSize / 2f;

        if (isPlayer)
        {
            gameObject.layer = LayerMask.NameToLayer("Player");
        }

        IsSetup = true;
    }

    private void CreateTilemapShipComponents(ShipTilemap t, bool isPlayer)
    {

        health = new Health(100, 0.01f);
        shield = new Shield(0, 0f, 0f);
        armourQuadrants = new Armour[4];
        Vector2[] directions = new Vector2[] { Vector2.up, Vector2.right, Vector2.down, Vector2.left };
        for (int i = 0; i < armourQuadrants.Length; i++)
        {
            armourQuadrants[i] = new Armour(100, 0.01f, directions[i]);
        }


        Tilemap map = t.GetComponent<Tilemap>();
        List<ShipThruster> thrusterList = new List<ShipThruster>();
        List<WeaponMono> weaponList = new List<WeaponMono>();
        for (int x = -t.editDistanceAroundOrigin.x; x < t.editDistanceAroundOrigin.x+1;  x++)
        {
            for (int y = -t.editDistanceAroundOrigin.y; y < t.editDistanceAroundOrigin.y+1; y++)
            {
                TileBase tile = map.GetTile(new Vector3Int(x, y, 0));
                if (tile != null)
                {
                    if (tile == t.HullTile)
                    {
                        Debug.Log("x:" + x + "  y:" + y + " HullTile " + tile);
                    }
                    else if (tile == t.WeaponTile)
                    {
                        Debug.Log("x:" + x + "  y:" + y + " WeaponTile " + tile);
                        weaponList.Add(CreateShipWeapon(map, new Vector3Int(x, y, 0), isPlayer, WeaponControllerType.BULLET));
                    }
                    else if (tile == t.EngineTile)
                    {
                        Debug.Log("x:" + x + "  y:" + y + " EngineTile " + tile);
                        thrusterList.Add(CreateShipThruster(map, new Vector3Int(x, y, 0)));
                    }
                    else if (tile == t.ShieldTile)
                    {
                        shield.max += 25;
                        shield.SetCurrent(shield.max);
                        shield.repairRate += 0.0025f;
                        shield.chargeRate += 0.025f;
                    }
                    else if (tile == t.BeamTile)
                    {
                        weaponList.Add(CreateShipWeapon(map, new Vector3Int(x, y, 0), isPlayer, WeaponControllerType.BEAM));
                    }
                }
            }
        }
        shipThrusters = thrusterList.ToArray();
        weaponSlots = weaponList.ToArray();
    }

    public ShipThruster CreateShipThruster(Tilemap map, Vector3Int tilePos)
    {
        ShipThruster particles = Instantiate(thrusterPrefab, transform);
        particles.transform.position = map.GetCellCenterWorld(tilePos);

        Matrix4x4 tileTransform = map.GetTransformMatrix(tilePos);
        
        Quaternion engineTileRotation = tileTransform.rotation;

        Vector2 thrustDirection = engineTileRotation * Vector2.up;

        float thrusterPower = 1f;
        particles.Setup(thrustDirection, thrusterPower);

        return particles;
    }

    public WeaponMono CreateShipWeapon(Tilemap map, Vector3Int tilePos, bool isPlayer, WeaponControllerType type)
    {

        Matrix4x4 tileTransform = map.GetTransformMatrix(tilePos);
        Quaternion tileRotation = tileTransform.rotation;

        WeaponMono weapon = Instantiate(shipWeaponPrefab, transform);
        weapon.transform.position = map.GetCellCenterWorld(tilePos);
        weapon.transform.rotation = tileRotation * Quaternion.Euler(0,0,180);

        weapon.Setup(weaponGen.Generate(type), GetComponent<Rigidbody2D>(), isPlayer);

        return weapon;
    }

    //public void CreateShipThrusters()
    //{
    //    int numberOfThrusters = 1;

    //    //thrusterPositions = new ThrusterPosition[3];

    //    GameObject[] thrusterParticles = new GameObject[numberOfThrusters];
    //    Vector2 extents = GetComponent<Renderer>().bounds.extents;

    //    Vector2 thrusterDirection = Vector2.up;

    //    for (int i = 0; i < thrusterParticles.Length; i++)
    //    {
    //        CreateThruster(numberOfThrusters, thrusterParticles, extents, i);

    //    }

    //    //thrusterPositions[1] = new ThrusterPosition(new Vector2(-extents.x, 0), Vector2.left);
    //    //thrusterPositions[2] = new ThrusterPosition(new Vector2(extents.x, 0), Vector2.right);
    //}

    //private void CreateThruster(int numberOfThrusters, GameObject[] thrusterParticles, Vector2 extents, int i)
    //{
    //    ShipThruster particles = Instantiate(thrusterPrefab, transform);

    //    particles.transform.localPosition = Vector2.zero;

    //    Vector2 localThrusterPos = Vector2.zero;

    //    if (numberOfThrusters < 2)
    //    {
    //        //localThrusterPos += new Vector2(0, -extents.y);
    //        particles.transform.position += new Vector3(0, -extents.y);
    //    }
    //    else
    //    {
    //        //localThrusterPos += new Vector2(Mathf.Lerp(-extents.x, extents.x, (float)i / (thrusterPositions.Length - 1)), -extents.y);
    //        particles.transform.position += new Vector3(Mathf.Lerp(-extents.x, extents.x, (float)i / (thrusterParticles.Length - 1)), -extents.y);
    //    }

    //    //thrusterPositions[i] = new ThrusterPosition(particles.transform.localPosition, thrusterDirection); 
    //    //start here, how to apply force to ship in multiple directions
    //    // start with looking at it from a view of, I provide instruction to go forwards, ship will respond by attempting to get a forwards force from each thruster.
    //    // all thrusters will give the forward component of their thrust, which might be zero if they are at 90' to the ship.
    //    // use dot product to get ratio of force in direction.
    //    thrusterParticles[i] = particles;
    //}

    //public void CreateWeaponSlots()
    //{
    //    int numberOfSlots = 1;
    //    weaponSlots = new GameObject[numberOfSlots];

    //    for (int i = 0; i < weaponSlots.Length; i++)
    //    {
    //        GameObject weaponSlot = new GameObject("WeaponSlot");

    //        Vector2 extents = GetComponent<Renderer>().bounds.extents;

    //        weaponSlot.transform.SetParent(gameObject.transform);
    //        weaponSlot.transform.localPosition = Vector2.zero;

    //        if (weaponSlots.Length < 2)
    //        {
    //            weaponSlot.transform.position += new Vector3(0, extents.y);
    //        }
    //        else
    //        {
    //            weaponSlot.transform.position += new Vector3(Mathf.Lerp(-extents.x, extents.x, (float)i / (weaponSlots.Length - 1)), extents.y);
    //        }

    //        weaponSlots[i] = weaponSlot;
    //    }
    //}

    //private void FillWeaponSlotsWithRandomWeapons(bool isPlayer)
    //{
    //    for (int i = 0; i < weaponSlots.Length; i++)
    //    {
    //        FillWeaponSlotWithRandomWeapons(weaponSlots[i], isPlayer);
    //    }
    //}

    //private void FillWeaponSlotWithRandomWeapons(GameObject slot, bool isPlayer)
    //{
    //    //GameObject weapon = new GameObject("weapon");

    //    WeaponMono weapon = Instantiate(shipWeaponPrefab);

    //    weapon.Setup(weaponGen.Generate(), GetComponent<Rigidbody2D>(), isPlayer);

    //    //weapon.transform.localScale = new Vector3(4, 4, 4);
    //    SpriteRenderer rend = weapon.GetComponent<SpriteRenderer>();
    //    rend.sprite = ShipGenerator.GenerateWeaponSprite();
    //    //rend.sortingLayerName = "Foreground";

    //    weapon.transform.SetParent(slot.transform);
    //    weapon.transform.localPosition = Vector2.zero;
    //}

    public struct ThrusterPosition
    {
        public Vector2 point;
        public Vector2 direction;

        public ThrusterPosition(Vector2 point, Vector2 direction)
        {
            this.point = point;
            this.direction = direction;
        }
    }

    //public ThrusterPosition[] GetThrusterPositions()
    //{
    //    return thrusterPositions;
    //}

    //public void SetThrusterPositions(ThrusterPosition[] positions)
    //{
    //    thrusterPositions = positions;
    //}

    ShipSize GetSize()
    {
        return size;
    }
}
