﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForegroundFogController : MonoBehaviour {

    Texture2D backgroundTexture;
    MeshRenderer meshRenderer;

    FollowCamera followCam;

    // Use this for initialization
    void Start()
    {
        followCam = Camera.main.GetComponent<FollowCamera>();

        //rend = GetComponent<Renderer>();
        meshRenderer = GetComponent<MeshRenderer>();
        //Debug.Log(Camera.main.scaledPixelWidth + "  " + Camera.main.scaledPixelHeight);
        backgroundTexture = new Texture2D(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight);
        Color pixelColour = Color.white;

        int cropFactor = 1;
        for (int x = 0; x < backgroundTexture.width; x += cropFactor)
        {
            for (int y = 0; y < backgroundTexture.height; y += cropFactor)
            {

                //if (x % cropFactor == 0 && y % cropFactor == 0)
                //{
                //    //float nx = x + (x / backgroundTexture.width) - 0.5f;
                //    //float ny = y + (y / backgroundTexture.height) - 0.5f;
                //    float noiseVal = Mathf.PerlinNoise(x * 0.02f, y * 0.02f);
                //    //noiseVal = (noiseVal + 0.1f) / (0.1f / 2f); // collapse noise towards grey.
                //    pixelColour = new Color(noiseVal, noiseVal, noiseVal, (1 - noiseVal) / 2);
                //}

                float x1, x2, y1, y2;
                x1 = y1 = 0f;
                x2 = y2 = 5f;

                float s = (float) x / backgroundTexture.width;
                float t = (float) y / backgroundTexture.height;

                float dx = x2 - x1;
                float dy = y2 - y1;

                float nx = x1 + Mathf.Cos(s * 2f * Mathf.PI) * dx / (2f * Mathf.PI);
                float ny = y1 + Mathf.Cos(t * 2f * Mathf.PI) * dy / (2f * Mathf.PI);
                float nz = x1 + Mathf.Sin(s * 2f * Mathf.PI) * dx / (2f * Mathf.PI);
                float nw = y1 + Mathf.Sin(t * 2f * Mathf.PI) * dy / (2f * Mathf.PI);

                //float noiseVal = Mathf.PerlinNoise(x * 0.02f, y * 0.02f);
                //noiseVal = (noiseVal + 0.1f) / (0.1f / 2f); // collapse noise towards grey.

                float noiseVal = (float)SimplexNoise.noise(nx, ny, nz, nw);

                pixelColour = new Color(noiseVal, noiseVal, noiseVal, (1 - noiseVal) / 4);
                //buffer: set(x, y, Noise4D(nx, ny, nz, nw));

                backgroundTexture.SetPixel(x, y, pixelColour);

            }
        }

        backgroundTexture.Apply();
        //backgroundTexture.filterMode = FilterMode.Point;
        backgroundTexture.wrapMode = TextureWrapMode.Repeat;

        //Sprite perlinBackground = Sprite.Create(backgroundTexture, new Rect(0, 0, backgroundTexture.width, backgroundTexture.height), new Vector2(0.5f, 0.5f));
        //meshRenderer.sprite = perlinBackground;

        meshRenderer.material.mainTexture = backgroundTexture;
    }

    // Update is called once per frame
    void Update () {
        //if (followCam.HasTarget())
        //{
        //    target = followCam.target;
        //}
    }

    private void LateUpdate()
    {
        if (followCam.HasTarget())
        {
            meshRenderer.material.mainTextureOffset = followCam.transform.position / 16;
        }
    }
}
