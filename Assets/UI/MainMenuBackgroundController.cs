﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuBackgroundController : MonoBehaviour {

    Texture2D backgroundTexture;
    MeshRenderer meshRenderer;

	// Use this for initialization
	void Start () {
        //rend = GetComponent<Renderer>();
        meshRenderer = GetComponent<MeshRenderer>();

        backgroundTexture = new Texture2D(Camera.main.scaledPixelWidth, Camera.main.scaledPixelHeight);
        Color pixelColour = Color.white;

        int cropFactor = 16;
        for (int x = 0; x < backgroundTexture.width; x += cropFactor)
        {
            for (int y = 0; y < backgroundTexture.height; y += cropFactor)
            {
                
                if (x % cropFactor == 0 && y % cropFactor == 0)
                {
                    float nx = x + (x / backgroundTexture.width) - 0.5f;
                    float ny = y + (y / backgroundTexture.height) - 0.5f;
                    float noiseVal = Mathf.PerlinNoise(0.25f * nx, 0.25f * ny) + (0.25f * Mathf.PerlinNoise(2 * nx, 2 * ny)) + (0.1f * Mathf.PerlinNoise(4 * nx, 4 * ny));
                    pixelColour = new Color(noiseVal, noiseVal, noiseVal);
                }

                for (int xi = 0; xi < cropFactor; xi++)
                {
                    for (int yi = 0; yi < cropFactor; yi++)
                    {
                        backgroundTexture.SetPixel(x + xi, y + yi, pixelColour);
                    }
                }
                
            }
        }
        
        backgroundTexture.Apply();
        backgroundTexture.filterMode = FilterMode.Point;
        backgroundTexture.wrapMode = TextureWrapMode.Repeat;

        //Sprite perlinBackground = Sprite.Create(backgroundTexture, new Rect(0, 0, backgroundTexture.width, backgroundTexture.height), new Vector2(0.5f, 0.5f));
        //meshRenderer.sprite = perlinBackground;

        meshRenderer.material.mainTexture = backgroundTexture;
    }

    // Update is called once per frame
    void LateUpdate () {
        //meshRenderer.material.mainTextureOffset += new Vector2(0.006f, 0);
        //rend.material.mainTextureOffset += Vector2.up;
    }
}
