﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponMono : MonoBehaviour {

    //PlayerWeaponController WeaponController;
    private Weapon weapon;

    private WeaponStateManager stateManager;

    enum InteractionType { PRESSED, HELD, RELEASED }

    public string ActivationButtonName { get; private set; }

    Vector2 missileFollowTarget;

    BeamRenderer beamRenderer;

    ObjectPool projectilePool;

    Rigidbody2D parentShipRigidbody;
    bool parentShipIsPlayer;
    string weaponLayer;

    private void Awake()
    {
        beamRenderer = GetComponentInChildren<BeamRenderer>();

    }

    // Use this for initialization
    void Start () {
        
    }

    public void Setup(Weapon w, Rigidbody2D parentRigidbody, bool parentShipIsPlayer)
    {
        SetWeapon(w);
        this.stateManager = new WeaponStateManager(w.chargeTime, w.cooldownTime, w.shootPattern.amount, w.shootPattern.delayBetweenProjectiles);

        projectilePool = GameObject.Find("GlobalProjectilePool").GetComponent<ObjectPool>();

        this.parentShipRigidbody = parentRigidbody;
        this.parentShipIsPlayer = parentShipIsPlayer;
        weaponLayer = parentShipIsPlayer ? "PlayerBullet" : "EnemyBullet";
    }

    public void SetWeapon(Weapon w)
    {
        beamRenderer.line.enabled = false;
        weapon = w;
        if (weapon.controlType == WeaponControllerType.BULLET)
        {
            ActivationButtonName = "Fire1";
        }
        else if (weapon.controlType == WeaponControllerType.BEAM)
        {
            ActivationButtonName = "Fire1";
            beamRenderer.line.enabled = true;
        }
        else if (weapon.controlType == WeaponControllerType.MISSILE)
        {
            ActivationButtonName = "Fire3";
        }
    }

    public Weapon GetWeapon()
    {
        return weapon;
    }
	
	// Update is called once per frame
	void Update () {
        UpdateTimers(Time.deltaTime, Input.GetButton(ActivationButtonName));
    }

    public void UpdateTimers(float deltaTime, bool fireButtonHeld)
    {
        //Debug.Log(stateManager.GetCurrentState());
        if (stateManager.IsShooting())
        {
            if (weapon.shootPattern.delayBetweenProjectiles <= 0f)
            {
                for (int i = 0; i < weapon.shootPattern.amount; i++)
                {
                    Shoot();
                    stateManager.SetToCooldown();
                }
            }
            else
            {
                Shoot();
            }
        }
        stateManager.UpdateTimers(deltaTime, fireButtonHeld);
    }

    void AttemptToShoot(InteractionType type)
    {
        if (stateManager.IsReady())
        {
            if (type == InteractionType.PRESSED)
            {
                stateManager.AdvanceState();
            }
            else if (type == InteractionType.HELD && weapon.repeatsWhileButtonHeld)
            {
                stateManager.AdvanceState();
            }
        }
        if (stateManager.IsCharging())
        {
            if (type == InteractionType.RELEASED)
            {
                // stop charging if button not held.
                stateManager.RewindState();
            }
        }
        if (stateManager.IsCharged())
        {
            if (!weapon.shootsOnButtonRelease)
            {
                stateManager.AdvanceState();
            }
            else if (type == InteractionType.RELEASED)
            {
                stateManager.AdvanceState();
            }
        }
        if (stateManager.IsShooting() || stateManager.IsWaitingForRepeat() || stateManager.IsOnCooldown())
        {
            if (type == InteractionType.PRESSED)
            {
                if (weapon.mustBeCharged)
                {
                    stateManager.SetToNoRechargeCooldown();
                }
            }
        }
    }

    private void Shoot()
    {
        if (weapon.controlType == WeaponControllerType.BULLET)
        {
            InstantiateBullet();
        }
        else if (weapon.controlType == WeaponControllerType.MISSILE)
        {
            InstantiateMissile();
        }
        else if (weapon.controlType == WeaponControllerType.BEAM)
        {
            ShootBeam();
        }
    }

    public void ShootButtonPressed()
    {
        AttemptToShoot(InteractionType.PRESSED);
    }

    public void ShootButtonHeld()
    {
        AttemptToShoot(InteractionType.HELD);
    }

    public void ShootButtonReleased()
    {
        AttemptToShoot(InteractionType.RELEASED);
    }

    public void InstantiateBullet()
    {
        GameObject projectile;

        projectile = projectilePool.RequestObject<BulletController>();
        ProjectileController baseController = projectile.GetComponent<ProjectileController>();

        // Produces a spread angle biased towards zero degrees. 
        // Weapon spread forms a distribution around zero
        // rather than uniform innaccuracy.
        float randomBiasedToZero = Random.Range(0, weapon.shootPattern.spreadAngle) - Random.Range(0, weapon.shootPattern.spreadAngle);
        Quaternion spreadQuat = Quaternion.Euler(new Vector3(0, 0, randomBiasedToZero));

        baseController.Setup(parentShipRigidbody, weapon, transform.position, Quaternion.LookRotation(transform.forward, transform.up) * spreadQuat);

        projectile.GetComponent<BulletController>().BeShot(baseController, parentShipRigidbody.velocity);

        projectile.GetComponent<SpriteRenderer>().sprite = weapon.projectileSprite;

        projectile.layer = LayerMask.NameToLayer(weaponLayer);
    }

    public void InstantiateMissile()
    {
        GameObject projectile;

        projectile = projectilePool.RequestObject<MissileController>();
        ProjectileController baseController = projectile.GetComponent<ProjectileController>();
        baseController.Setup(parentShipRigidbody, weapon, transform.position, Quaternion.LookRotation(transform.forward, transform.up));

        projectile.GetComponent<MissileController>().BeShot(baseController, parentShipRigidbody.velocity + (Random.insideUnitCircle.normalized), Helper.GetWorldPositionOnPlane(Input.mousePosition, transform.position.z));

        projectile.GetComponent<SpriteRenderer>().sprite = weapon.projectileSprite;

        projectile.layer = LayerMask.NameToLayer(weaponLayer);
    }

    public void ShootBeam()
    {
        float beamLength = 1;

        beamRenderer.SetColour(Color.red);
        beamRenderer.SetLength(beamLength);

        Invoke("HideBeam", weapon.lifeTime);


    }

    void HideBeam()
    {
        beamRenderer.SetColour(Color.clear);
    }

    public class WeaponStateManager
    {
        public enum WeaponState { READY, CHARGING, CHARGED, SHOOTING, WAITING_FOR_REPEAT, COOLDOWN, NO_RECHARGE_COOLDOWN }

        WeaponState currentState; // need to add another cooldown state that doesn't allow "recharging" of weapon.
        // go to this state when button released during shooting (how to do this without cancelling the shooting of a burst? another state?)

        // In seconds
        float currentTime;
        float chargeTime;
        float cooldownTime;

        int numberOfShoots;
        float shootInterval;
        float shootingTime;
        int shootsRemaining;

        bool moveToNoRechargeCooldownAfterShootingFinished;

        public WeaponStateManager(float chargeTime, float cooldownTime, int numberOfShoots, float shootInterval)
        {
            currentState = WeaponState.READY;
            currentTime = 0;
            this.chargeTime = chargeTime;
            this.cooldownTime = cooldownTime;

            this.numberOfShoots = numberOfShoots;
            this.shootInterval = shootInterval;
            this.shootingTime = numberOfShoots * shootInterval;
        }

        public WeaponState GetCurrentState()
        {
            return currentState;
        }

        public void UpdateTimers(float deltaTime, bool fireButtonHeld)
        {

            //Debug.Log(currentState);

            if (IsCharging() && currentTime + deltaTime > chargeTime)
            {
                AdvanceState(); // Advance to CHARGED
                currentTime = 0;
            }
            else if (IsOnCooldown() && currentTime + deltaTime > cooldownTime)
            {

                if (chargeTime > 0 && fireButtonHeld) // Holding down a charge
                {
                    currentState = WeaponState.CHARGED;
                }
                else
                {
                    AdvanceState(); // Advance to READY
                }

                currentTime = 0;
            }
            else if (IsOnNoRechargeCooldown() && currentTime + deltaTime > cooldownTime)
            {
                AdvanceState();
            }
            else if (IsShooting())
            {
                shootsRemaining -= 1;
                AdvanceState();
                currentTime = 0;
            }
            else if (IsWaitingForRepeat() && currentTime + deltaTime > shootInterval)
            {
                AdvanceState();
                currentTime = 0;
            }
            else
            {
                currentTime += deltaTime;
            }
            //Debug.Log(currentState + "   " + currentTime + "   " + chargeTime + "  " + cooldownTime);
        }

        public void AdvanceState()
        {
            currentTime = 0;

            if (IsReady())
            {
                currentState = WeaponState.CHARGING;
            }
            else if (IsCharging())
            {
                currentState = WeaponState.CHARGED;
            }
            else if (IsCharged())
            {
                shootsRemaining = numberOfShoots;
                currentState = WeaponState.SHOOTING;
            }
            else if (IsShooting())
            {
                if (shootsRemaining > 0)
                {
                    currentState = WeaponState.WAITING_FOR_REPEAT;
                }
                else
                {
                    currentState = WeaponState.COOLDOWN;
                }
            }
            else if (IsWaitingForRepeat())
            {
                currentState = WeaponState.SHOOTING;
            }
            else if (IsOnCooldown())
            {
                currentState = WeaponState.READY;
            }
            else if (IsOnNoRechargeCooldown())
            {
                currentState = WeaponState.READY;
            }
        }

        public void RewindState() // Not complete for all states
        {
            currentTime = 0;
            if (IsReady())
            {
                currentState = WeaponState.READY;
            }
            else if (IsCharging())
            {
                currentState = WeaponState.READY;
            }
            else if (IsCharged())
            {
                currentState = WeaponState.CHARGING;
            }
            else if (IsShooting())
            {
                currentState = WeaponState.CHARGED;
            }
            else if (IsOnCooldown())
            {
                currentState = WeaponState.SHOOTING;
            }
        }

        public bool IsReady()
        {
            return currentState == WeaponState.READY;
        }

        public bool IsCharging()
        {
            return currentState == WeaponState.CHARGING;
        }

        public bool IsCharged()
        {
            return currentState == WeaponState.CHARGED;
        }

        public bool IsShooting()
        {
            return currentState == WeaponState.SHOOTING;
        }

        public bool IsWaitingForRepeat()
        {
            return currentState == WeaponState.WAITING_FOR_REPEAT;
        }

        public bool IsOnCooldown()
        {
            return currentState == WeaponState.COOLDOWN;
        }

        public bool IsOnNoRechargeCooldown()
        {
            return currentState == WeaponState.NO_RECHARGE_COOLDOWN;
        }

        public void SetToCooldown()
        {
            currentState = WeaponState.COOLDOWN;
        }

        public void SetToNoRechargeCooldown()
        {
            currentState = WeaponState.NO_RECHARGE_COOLDOWN;
        }
    }

}
