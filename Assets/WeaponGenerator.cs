﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGenerator
{

    public Weapon Generate(WeaponControllerType type)
    {
        
        if (type == WeaponControllerType.BEAM)
        {
             return new Weapon(1, 3f, 1f, 60f, 4f, new ProjectilePattern(1, 0, 0f), true, false, WeaponControllerType.BEAM);
        }
        else if (type == WeaponControllerType.MISSILE)
        {
             return new Weapon(15, 0.5f, 1f, 60f, 0.7f, new ProjectilePattern(1, 0, 0f), true, false, WeaponControllerType.MISSILE);
        }
        else
        {
            return new Weapon(15, 0.5f, 1f, 60f, 0.7f, new ProjectilePattern(1, 0, 0f), true, false, WeaponControllerType.BULLET);
        }
        //Weapon weapon = new Weapon(15, 4f, 0.8f, 0f, 3f, new ProjectilePattern(1, 0, 0f), true, false, WeaponControllerType.BEAM);
    }

    public static int[] BulletMask = new int[]
    {
        0,0,0,2,
        0,0,2,2,
        0,3,2,2,
        4,2,1,1,
        4,2,1,1,
        0,3,2,2,
        0,0,4,2,
        0,0,0,4,
    };
}
