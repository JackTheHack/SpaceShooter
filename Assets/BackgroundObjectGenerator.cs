﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundObjectGenerator : MonoBehaviour {

    public GameObject backgroundPlanetPrefab;

    public int numberOfPlanets = 5;
    public float planetZPosition = 50;

	// Use this for initialization
	void Start () {
       
        for (int i = 0; i < numberOfPlanets; i++)
        {
            Sprite planetSprite = GeneratePlanetSprite();
            GameObject planet = Instantiate(backgroundPlanetPrefab, new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), planetZPosition + Random.Range(-10, 100)), Quaternion.identity);
            planet.transform.localScale += (new Vector3(1,1,0) * Random.Range(0, 5));
            planet.GetComponent<SpriteRenderer>().sprite = planetSprite;
        }

        
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite GeneratePlanetSprite()
    {
        int planetSize = Random.Range(256, 512);
        Color[] pixels = new Color[planetSize * planetSize];

        float planetRadius = planetSize / 2;

        float planetCloudEffectStrength = Random.Range(2f, 20f);

        float hue = Random.value;

        float xOrg = Random.Range(0, 1000);
        float yOrg = Random.Range(0, 1000);

        for (int y = 0; y < planetSize; y++)
        {
            for (int x = 0; x < planetSize; x++)
            {
                Color choice = Color.clear;

                if (Mathf.Pow(x - planetRadius, 2) + 1.1f*Mathf.Pow(y - planetRadius, 2) < planetRadius * planetRadius)
                {
                    float nx = xOrg + ((float)x / planetSize) ;
                    float ny = yOrg + ((float)y / planetSize) ;
                    float noiseVal = Mathf.PerlinNoise( nx, planetCloudEffectStrength * ny );
                    choice = Color.HSVToRGB(hue, noiseVal + 0.5f, noiseVal + 0.2f);
                    
                }

                pixels[(planetSize * y) + x] = choice;
            }
        }

        return SpriteGenerator.GetSpriteFromPixels(planetSize, planetSize, pixels);
    }
}
