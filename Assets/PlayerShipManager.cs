﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShipManager : MonoBehaviour {

    public Ship currentlyPiloted { get; private set; }
    WeaponMono[] currentShipWeapons;

    public Ship[] pilotableShips { get; private set; } 

    PlayerMovementController movementController;

    ShipGenerator shipGen;

    public Ship tilemapTestShip;

    // Use this for initialization
    void Start () {

        pilotableShips = new Ship[3];

        shipGen = GetComponent<ShipGenerator>();

        //tilemapTestShip.Setup(ShipSize.SMALL, Sprite.Create(, 3, 3, true);
        //pilotableShips[0] = tilemapTestShip;

        //pilotableShips[0] = shipGen.GenerateRandomPlayerShip();
        //pilotableShips[1] = shipGen.GenerateRandomPlayerShip();
        //pilotableShips[2] = shipGen.GenerateRandomPlayerShip();

        //pilotableShips[0].gameObject.layer = LayerMask.NameToLayer("Player");
        //pilotableShips[1].gameObject.layer = LayerMask.NameToLayer("Player");
        //pilotableShips[2].gameObject.layer = LayerMask.NameToLayer("Player");

        movementController = gameObject.AddComponent<PlayerMovementController>();
        //SwitchShipUnderControl(0);
	}
	
	// Update is called once per frame
	void Update () {
		
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            SwitchShipUnderControl(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            SwitchShipUnderControl(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            SwitchShipUnderControl(2);
        }

        if (currentlyPiloted != null)
        {
            for (int i = 0; i < currentShipWeapons.Length; i++)
            {
                if (Input.GetButtonDown(currentShipWeapons[i].ActivationButtonName))
                {
                    currentShipWeapons[i].ShootButtonPressed();
                }
                else if (Input.GetButton(currentShipWeapons[i].ActivationButtonName))
                {
                    currentShipWeapons[i].ShootButtonHeld();
                }
                else if (Input.GetButtonUp(currentShipWeapons[i].ActivationButtonName))
                {
                    currentShipWeapons[i].ShootButtonReleased();
                }
            }
        }
    }

    public void SetupBuiltShip(Ship ship)
    {
        ShipTilemap map = ship.GetComponent<ShipTilemap>();
        map.SetInBuildMode(false);
        ship.Setup(ShipSize.SMALL, map, 3, 3, true);
        ship.GetComponent<UnityEngine.Tilemaps.TilemapCollider2D>().enabled = true;
        SetControlledShip(ship);
    }

    public void SetControlledShip(Ship ship)
    {
        currentlyPiloted = ship;
        movementController.Setup(currentlyPiloted);
        currentShipWeapons = currentlyPiloted.GetComponentsInChildren<WeaponMono>();
    }

    public void SwitchShipUnderControl(int shipIndex)
    {
        if (shipIndex < 0 && shipIndex >= pilotableShips.Length)
        {
            Debug.LogError("Tried to switch ship with invalid ship index: " + shipIndex);
            return;
        }

        currentlyPiloted = pilotableShips[shipIndex];

        movementController.Setup(currentlyPiloted);

        currentShipWeapons = currentlyPiloted.GetComponentsInChildren<WeaponMono>();
    }
}
