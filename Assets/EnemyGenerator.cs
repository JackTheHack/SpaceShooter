﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour {

    ObjectPool shipGenerator;
    int livingEnemies;

    public int livingEnemyCap = 1;

	// Use this for initialization
	void Start () {
        shipGenerator = GetComponent<ObjectPool>();
        shipGenerator.AddNewPoolType<EnemyShipAI>();
        livingEnemies = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (livingEnemies < livingEnemyCap)
        {
            GameObject ship = shipGenerator.RequestObject<EnemyShipAI>();
            livingEnemies += 1;

            ship.layer = LayerMask.NameToLayer("Enemies");

            //ship.GetComponent<Ship>().Setup(ShipSize.SMALL, ShipGenerator.GenerateRandomPlayerSprite(ShipSize.SMALL), Random.Range(2f, 2f), Random.Range(6f, 6f), false);
        }
	}
}
