﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour
{

    public Rigidbody2D parentRigidbody; // The rigidbody of the gun that this is to be fired from, for doing velocity inheritance.
    public Weapon parentWeapon;
    protected Rigidbody2D rb2d;

    SpriteRenderer spriteRenderer;
    Color initialColour;

    protected float lifeElapsed = 0f;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        initialColour = spriteRenderer.color;
    }

    // Use this for initialization
    void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        FadeOut();
        if (gameObject.activeInHierarchy && lifeElapsed >= parentWeapon.lifeTime)
        {
            
            Deactivate();
        }
        else
        {
            lifeElapsed += Time.deltaTime;
        }
    }

    public void Setup(Rigidbody2D parentRigidbody, Weapon parent, Vector3 gunPosition, Quaternion playerRotation)
    {
        this.parentRigidbody = parentRigidbody;
        parentWeapon = parent;
        transform.position = gunPosition;
        transform.rotation = playerRotation;
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
        lifeElapsed = 0;
    }

    private void FadeOut()
    {
        spriteRenderer.color = Color.Lerp(initialColour, Color.clear, (lifeElapsed*lifeElapsed*lifeElapsed) / parentWeapon.lifeTime);
    }

}
