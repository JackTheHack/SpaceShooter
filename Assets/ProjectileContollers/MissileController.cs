﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileController : MonoBehaviour
{

    ProjectileController baseController;

    bool isFollowing;
    Vector2 followTarget;

    //Vector2 initialPush;
    Rigidbody2D rb2d;

    public void BeShot(ProjectileController baseController, Vector2 inheritedVelocity, Vector2 followTarget)
    {
        this.baseController = baseController;
        //this.initialPush = inheritedVelocity;
        this.followTarget = followTarget;

        isFollowing = false;
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = inheritedVelocity;
        rb2d.AddForce(baseController.parentRigidbody.transform.up * baseController.parentWeapon.projectileSpeed);

        Invoke("SetFollowing", 1.5f);
    }

    private void SetFollowing()
    {
        isFollowing = true;
    }

    private void FixedUpdate()
    {
        if (isFollowing)
        {
            FollowTarget(followTarget);
        }   
    }

    private void Update()
    {
        if (isFollowing)
        {
            //Vector2 between = ;
            //transform.up = new Vector3(between.x, between.y, -1);
            Vector2 dir = followTarget - (Vector2)transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;

            angle -= 90;

            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        }
    }

    void FollowTarget(Vector2 position)
    {
        rb2d.AddForce(position - (Vector2)transform.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Missile collided with object at: " + collision.transform.position);
        baseController.Deactivate();
    }

}
