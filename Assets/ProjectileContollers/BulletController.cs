﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{

    ProjectileController baseController;
    Rigidbody2D rb2d;

    //ContactPoint2D[] hitBuf = new ContactPoint2D[1];
    

    public void BeShot(ProjectileController baseController, Vector2 inheritedVelocity)
    {

        this.baseController = baseController;

        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = inheritedVelocity;
        rb2d.AddForce((baseController.transform.up * baseController.parentWeapon.projectileSpeed) );
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log("Bullet collided with object at: " + collision.transform.position);
        baseController.Deactivate();

        //int amount = collision.GetContacts(hitBuf);

        //Vector2 normal = hitBuf[0].normal;

        //DamageController.DamageAmountAndDirection damageStruct = new DamageController.DamageAmountAndDirection(baseController.parentWeapon.baseDamage, normal);

        //collision.gameObject.GetComponent<DamageController>().TakeDamage(damageStruct);
        //Debug.Log("Bullet did " + baseController.parentWeapon.baseDamage + " damage to " + collision.gameObject);
    }
}

