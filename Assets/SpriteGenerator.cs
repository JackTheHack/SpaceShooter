﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpriteGenerator {

    public static Color[] GetSymmetricalMaskedPixels(int maskWidth, int maskHeight, int[] mask, Color color1, Color color2, bool shade)
    {
        mask = ShoogleMask(mask, maskWidth);
        mask = GetMirroredArray(mask, maskWidth);

        return GenerateMaskPixels(maskWidth * 2, maskHeight, mask, color1, color2, shade);
    }

    public static Color[] GenerateMaskPixels(int width, int height, int[] mask, Color colour, Color colour2, bool shade)
    {
        Color[] pixels = new Color[mask.Length];
        for (int i = 0; i < mask.Length; i++)
        {
            pixels[i] = colour;
        }

        Color[] maskedPixels = ApplyMask(width, height, pixels, mask, colour, colour2, shade);
        return maskedPixels;
    }

    public static Sprite GetSymmetricalMaskedSprite(int maskWidth, int maskHeight, int[] mask, Color color1, Color color2, bool shade)
    {
        mask = ShoogleMask(mask, maskWidth);
        mask = GetMirroredArray(mask, maskWidth);

        return GenerateMaskSprite(maskWidth * 2, maskHeight, mask, color1, color2, shade);
    }

    public static Sprite GenerateMaskSprite(int width, int height, int[] mask, Color colour, Color colour2, bool shade)
    {
        Color[] pixels = GenerateMaskPixels(width, height, mask, colour, colour2, shade);
        return GetSpriteFromPixels(width, height, pixels);
    }

    public static Sprite GetMirroredSprite(int pixelsWidth, int pixelsHeight, Color[] halfPixels)
    {
        Color[] mirroredPixels = GetMirroredArray(halfPixels, pixelsWidth);
        return GetSpriteFromPixels(pixelsWidth * 2, pixelsHeight, mirroredPixels);
    }

    public static Sprite GetSpriteFromPixels(int width, int height, Color[] pixels)
    {
        Texture2D tex = new Texture2D(width, height);

        System.Array.Reverse(pixels);

        tex.filterMode = FilterMode.Point;

        tex.SetPixels(pixels);
        tex.Apply();
        Sprite newSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
        return newSprite;
    }


    static Color[] ApplyMask(int width, int height, Color[] pixels, int[] mask, Color col1, Color col2, bool shade)
    {
        var directions = System.Enum.GetValues(typeof(Direction));
        var shadeCol = GetSimilarColour(col1);

        for (int i = 0; i < pixels.Length; i++)
        {
            if (mask[i] == 0)
            {
                pixels[i] = col2;
            }

            if (shade)
            {
                var outerPixel = false;
                foreach (Direction d in directions)
                {
                    if (mask[i] != 0 && i / width == 0)
                    {
                        outerPixel = true;
                        break;
                    }
                    if (mask[i] != 0 && i / width == height - 1)
                    {
                        outerPixel = true;
                        break;
                    }

                    int adjacent = i + GetArrayDirectionalStep(d, width);
                    if (adjacent > 0 && adjacent < mask.Length - 1)
                    {
                        if (mask[i + GetArrayDirectionalStep(d, width)] == 0)
                        {
                            outerPixel = true;
                            break;
                        }
                    }

                }

                if (!outerPixel)
                {
                    pixels[i] = shadeCol;
                    continue;
                }
            }
        }
        return pixels;
    }

    enum Direction { LEFT, RIGHT, UP, DOWN, UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT }

    static int GetArrayDirectionalStep(Direction d, int arrayWidth)
    {
        switch (d)
        {
            case Direction.LEFT:
                return -1;
            case Direction.RIGHT:
                return 1;
            case Direction.UP:
                return -arrayWidth;
            case Direction.DOWN:
                return arrayWidth;
            case Direction.UPLEFT:
                return -arrayWidth - 1;
            case Direction.UPRIGHT:
                return -arrayWidth + 1;
            case Direction.DOWNLEFT:
                return arrayWidth - 1;
            case Direction.DOWNRIGHT:
                return arrayWidth + 1;
            default:
                Debug.Log("Invalid Direction in GetArrayDirectionalStep: " + d);
                return 0;
        }
    }

    public static int[] ShoogleMask(int[] mask, int width)
    {

        var directions = System.Enum.GetValues(typeof(Direction));

        for (var i = 0; i < mask.Length; i++)
        {

            if (mask[i] == 2)
            {
                continue;
            }

            List<int> validTransitions = new List<int>();
            if (mask[i] == 1)
            {

                foreach (Direction d in directions)
                {
                    var transformation = i + GetArrayDirectionalStep(d, width);

                    if (transformation >= 0 && transformation < mask.Length)
                    {
                        validTransitions.Add(transformation);
                    }
                }

                if (validTransitions.Count == 0)
                {
                    continue;
                }

                int transition = validTransitions[Random.Range(0, validTransitions.Count)];
                mask[i] = 0;
                mask[transition] = 2;

            }
            else if (mask[i] == 3)
            {
                foreach (Direction d in directions)
                {
                    if (d == Direction.LEFT || d == Direction.RIGHT)
                    {
                        var transformation = i + GetArrayDirectionalStep(d, width);
                        if (transformation >= 0 && transformation < mask.Length)
                        {
                            validTransitions.Add(transformation);
                        }
                    }
                }
                int transition = validTransitions[Random.Range(0, validTransitions.Count)];
                mask[i] = 0;
                mask[transition] = 2;
            }
            else if (mask[i] == 4)
            {
                foreach (Direction d in directions)
                {
                    if (d == Direction.UP || d == Direction.DOWN)
                    {
                        var transformation = i + GetArrayDirectionalStep(d, width);

                        if (transformation >= 0 && transformation < mask.Length)
                        {
                            validTransitions.Add(transformation);
                        }
                    }
                }
                int transition = validTransitions[Random.Range(0, validTransitions.Count)];
                mask[i] = 0;
                mask[transition] = 2;
            }
        }

        for (var i = 0; i < mask.Length; i++)
        {
            if (mask[i] == 5)
            {
                mask[i] = 0;
            }
        }

        return mask;
    }

    public static T[] GetMirroredArray<T>(T[] mask, int unmirroredMaskWidth)
    {
        List<T> mirror = new List<T>();
        for (var i = 0; i < mask.Length; i += unmirroredMaskWidth) // Goes down each row
        {
            List<T> row = new List<T>(unmirroredMaskWidth * 2);
            for (var j = 0; j < unmirroredMaskWidth; j++) // Goes along each row
            {
                row.Add(mask[i + j]);
            }

            var reversed = new List<T>();
            reversed.AddRange(row);
            reversed.Reverse();

            row.AddRange(reversed);

            mirror.AddRange(row);
        }
        return mirror.ToArray();
    }

    static Color GetSimilarColour(Color c)
    {
        float H, S, V;
        Color.RGBToHSV(c, out H, out S, out V);
        S += Random.Range(-0.4f, 0.4f);
        V += Random.Range(-0.4f, 0.4f);

        if (V == 0)
        {
            V = 0.3f;
        }

        if (S == 0)
        {
            S = 0.3f;
        }

        return Color.HSVToRGB(H, S, V);
    }

    public static Color GetRandomHSVColour(float lowHue, float highHue, float lowSat, float highSat, float lowValue, float highValue)
    {
        var h = Random.Range(lowHue, highHue);
        var s = Random.Range(lowSat, highSat);
        var v = Random.Range(lowValue, highValue);
        return Color.HSVToRGB(h, s, v);
    }
}
