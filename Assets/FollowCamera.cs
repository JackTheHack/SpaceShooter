﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {

    public Transform target;
    public Vector3 targetOffset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        if (HasTarget())
        {
            //Vector3 targetPosAtCameraZ = new Vector3(target.localPosition.x, target.localPosition.y, transform.position.z);
            Vector3 localShift = target.TransformPoint(targetOffset);
            transform.position = new Vector3(localShift.x, localShift.y, transform.position.z);
        }
    }

    public bool HasTarget()
    {
        return target != null;
    }
}
