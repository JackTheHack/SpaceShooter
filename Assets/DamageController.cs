﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour {

    public void TakeDamage(DamageAmountAndDirection damageStruct)
    {
        SendMessage("Damage", damageStruct);
    }

    public struct DamageAmountAndDirection
    {
        public float amount;
        public Vector2 direction;

        public DamageAmountAndDirection(float amount, Vector2 direction)
        {
            this.amount = amount;
            this.direction = direction;
        }
    }

}
