﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeamRenderer : MonoBehaviour {

    public LineRenderer line { get; private set; }
    public Transform attachedTo;
    public Transform target;

    LayerMask layerMask;
    RaycastHit2D[] hitResults = new RaycastHit2D[4];

    float currentLength = 0;

    private void Awake()
    {
        line = GetComponent<LineRenderer>();
    }

    // Use this for initialization
    void Start () {

        SetColour(Color.clear);
        line.positionCount = 2;

        line.SetPosition(0, Vector3.zero);
        line.SetPosition(1, Vector3.zero);

        //contactFilter = new ContactFilter2D();
        //contactFilter.SetLayerMask(LayerMask.GetMask(new string[] { "Enemies", "Destructible", "Shootable" }));
        layerMask = LayerMask.GetMask(new string[] { "Enemies", "Destructible", "Shootable" });
    }
	
	// Update is called once per frame
	void Update () {
        //line.SetPosition(0, attachedTo.position);
        //line.SetPosition(1, target.position);

        

        float distance = Vector3.Distance(line.GetPosition(0), line.GetPosition(1));
        line.GetComponent<Renderer>().material.mainTextureScale = new Vector2(distance * 2, 1);

        Vector2 lineStartWorld = line.transform.position + line.GetPosition(0);
        Vector2 lineEndWorld = (Vector2) line.transform.position + (currentLength * (Vector2) transform.up); //*(Vector2)line.GetPosition(1)

        int hitCount = Physics2D.LinecastNonAlloc(lineStartWorld, lineEndWorld, hitResults, layerMask);

        if (hitCount == 0)
        {
            line.SetPosition(1, new Vector3(0, currentLength, line.GetPosition(0).z));
        }
        else
        {
            float minDist = Mathf.Infinity;
            for (int i = 0; i < hitCount; i++)
            {
                float pointToShip = Vector2.Distance(hitResults[i].point, (Vector2)transform.position);
                if (pointToShip < minDist)
                {
                    minDist = pointToShip;
                }
            }
            
            line.SetPosition(1, new Vector3(0, minDist, line.GetPosition(0).z));

        }

        line.material.mainTextureOffset += Vector2.left * 0.02f;
    }

    public void SetColour(Color c)
    {
        Color color = c;
        Color emissionColor = color;

        float h, s, v;
        Color.RGBToHSV(color, out h, out s, out v);
        emissionColor = Color.HSVToRGB(h, 0.5f, 0.7f);


        line.GetComponent<Renderer>().material.SetColor("_Color", color);
        line.GetComponent<Renderer>().material.SetColor("_EmissionColor", emissionColor);
    }

    public void SetLength(float length)
    {
        currentLength = length;
    }
}
