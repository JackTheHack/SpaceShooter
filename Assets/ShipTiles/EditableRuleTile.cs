﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace UnityEngine
{

    [Serializable]
    [CreateAssetMenu]
    public class EditableRuleTile : RuleTile
    {

        //public override void GetTileData(Vector3Int position, ITilemap tileMap, ref TileData tileData)
        //{
        //    tileData.sprite = m_DefaultSprite;
        //    tileData.colliderType = m_DefaultColliderType;
        //    tileData.flags = TileFlags.LockTransform;
        //    tileData.transform = Matrix4x4.identity;

        //    foreach (TilingRule rule in m_TilingRules)
        //    {
        //        Matrix4x4 transform = Matrix4x4.identity;
        //        if (RuleMatches(rule, position, tileMap, ref transform))
        //        {
        //            switch (rule.m_Output)
        //            {
        //                case TilingRule.OutputSprite.Single:
        //                case TilingRule.OutputSprite.Animation:
        //                    tileData.sprite = rule.m_Sprites[0];
        //                    break;
        //                case TilingRule.OutputSprite.Random:
        //                    int index = Mathf.Clamp(Mathf.FloorToInt(GetPerlinValue(position, rule.m_PerlinScale, 100000f) * rule.m_Sprites.Length), 0, rule.m_Sprites.Length - 1);
        //                    tileData.sprite = rule.m_Sprites[index];
        //                    if (rule.m_RandomTransform != TilingRule.Transform.Fixed)
        //                        transform = ApplyRandomTransform(rule.m_RandomTransform, transform, rule.m_PerlinScale, position);
        //                    break;
        //            }
        //            tileData.transform = transform;
        //            tileData.colliderType = rule.m_ColliderType;
        //            break;
        //        }
        //    }
        //}

        public bool IsRotatable = false;

        TileData currentTileData;
        Matrix4x4 rotatedTransform = Matrix4x4.identity;
        Sprite currentSprite;
        Tile.ColliderType currentColliderType;

        bool wasRotated = false;

        public override void GetTileData(Vector3Int position, ITilemap tileMap, ref TileData tileData)
        {

            tileData.sprite = m_DefaultSprite;
            tileData.colliderType = m_DefaultColliderType;
            tileData.flags = tileMap.GetComponent<EditableTilemap>().tileHelperMap[position].flags;
            tileData.transform = Matrix4x4.identity;

            Matrix4x4 transform = Matrix4x4.identity;
            Sprite sprite = m_DefaultSprite;
            Tile.ColliderType colliderType = tileData.colliderType;

            EditableTilemap edMap = tileMap.GetComponent<EditableTilemap>();

            bool setTile = false;
            foreach (TilingRule rule in m_TilingRules)
            {
                if (RuleMatches(rule, position, tileMap.GetComponent<Tilemap>(), ref transform))
                {   
                    colliderType = rule.m_ColliderType;
                    if (!setTile)
                    {
                        switch (rule.m_Output)
                        {
                            case TilingRule.OutputSprite.Single:
                            case TilingRule.OutputSprite.Animation:
                                sprite = rule.m_Sprites[0];
                                break;
                            //case TilingRule.OutputSprite.Random:
                            //    int index = Mathf.Clamp(Mathf.FloorToInt(GetPerlinValue(position, rule.m_PerlinScale, 100000f) * rule.m_Sprites.Length), 0, rule.m_Sprites.Length - 1);
                            //    sprite = rule.m_Sprites[index];
                            //    if (rule.m_RandomTransform != TilingRule.Transform.Fixed)
                            //        transform = ApplyRandomTransform(rule.m_RandomTransform, transform, rule.m_PerlinScale, position);
                            //    break;
                        }
                        setTile = true;
                    }
                }
            }

            currentTileData = tileData;
            currentSprite = sprite;
            currentColliderType = colliderType;

            if (wasRotated)
            {
                //Debug.Log("seting transform to rotated " + rotatedTransform);
                SetTileData(ref tileData, sprite, rotatedTransform, colliderType);
                wasRotated = false;
            }
            else
            {
                //Debug.Log("seting transform to non rotated");
                SetTileData(ref tileData, sprite, transform, colliderType);
            }
            

        }

        public void SetTileData(ref TileData tileData, Sprite s, Matrix4x4 transform, Tile.ColliderType colliderType)
        {
            tileData.sprite = s;
            tileData.transform = transform;
            tileData.colliderType = colliderType;
        }

        public void TryRotate(Vector3Int position, Tilemap tilemap, TileBase tile, int angle)
        {
            if (!IsRotatable)
            {
                return;
            }
            if (RuleMatches(m_TilingRules[0], position, tilemap, angle))
            {
                Matrix4x4 transform = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, -angle), Vector3.one);
                tilemap.SetTransformMatrix(position, transform);
                //tilemap.SetTileFlags(position, TileFlags.LockTransform);
                rotatedTransform = transform;
                wasRotated = true;
            }
        }

        public override bool RuleMatches(TilingRule rule, Vector3Int position, Tilemap tilemap, ref Matrix4x4 transform)
        {
            // Check rule against rotations of 0, 90, 180, 270
            for (int angle = 0; angle <= (rule.m_RuleTransform == TilingRule.Transform.Rotated ? 270 : 0); angle += 90)
            {
                if (RuleMatches(rule, position, tilemap, angle))
                {
                    transform = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, -angle), Vector3.one);                    
                    return true;
                }
            }

            // Check rule against x-axis mirror
            if ((rule.m_RuleTransform == TilingRule.Transform.MirrorX) && RuleMatches(rule, position, tilemap, true, false))
            {
                transform = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(-1f, 1f, 1f));
                return true;
            }

            // Check rule against y-axis mirror
            if ((rule.m_RuleTransform == TilingRule.Transform.MirrorY) && RuleMatches(rule, position, tilemap, false, true))
            {
                transform = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(1f, -1f, 1f));
                return true;
            }

            return false;
        }
    }
}
