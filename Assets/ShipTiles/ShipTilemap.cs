﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ShipTilemap : EditableTilemap {

    public EditableRuleTile HullTile;
    public EditableRuleTile EngineTile;
    public EditableRuleTile WeaponTile;
    public EditableRuleTile ReactorTile;
    public EditableRuleTile StorageTile;
    public EditableRuleTile ShieldTile;
    public EditableRuleTile BeamTile;

    // Use this for initialization
    protected void Start () {
        Debug.Log("ShipTileMap");

        EditableRuleTile[] tiles = new EditableRuleTile[] { HullTile, EngineTile, WeaponTile, ReactorTile, StorageTile, ShieldTile, BeamTile };
        Setup(tiles);
	}
	
}
