﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using System.Linq;

public abstract class EditableTilemap : MonoBehaviour {

    Grid grid;
    Tilemap tilemap;

    EditableRuleTile[] tiles;

    private int selectedTileIndex;
    private TileBase selectedTileForPaint;

    public Vector2Int editDistanceAroundOrigin = new Vector2Int(5, 5);

    GameObject UITileSprite;

    Vector3Int lastTilePos;

    public Dictionary<Vector3Int, TileDataHelper> tileHelperMap;

    int currentAngle = 0;

    bool InBuildMode = true;

    public class TileDataHelper
    {
        public List<Matrix4x4> transforms;
        public int transformsIndex;
        public bool locked;
        public TileFlags flags;

        public TileDataHelper(bool locked, TileFlags flags)
        {
            transforms = new List<Matrix4x4>();
            this.transformsIndex = 0;
            this.locked = locked;
            this.flags = flags;
        }
    }

    //public class TileDataHelper : IEquatable<TileDataHelper>
    //{
    //    public Vector3Int position;
    //    public int ruleIndex;

    //    public TileDataHelper(Vector3Int position, int ruleIndex)
    //    {
    //        this.position = position;
    //        this.ruleIndex = ruleIndex;
    //    }

    //    public bool Equals(TileDataHelper other)
    //    {
    //        return position == other.position;
    //    }
    //}

    public void SetInBuildMode(bool value)
    {
        InBuildMode = value;

        Vector3Int currentTileCoordUnderMouse = GetTileCoordUnderMouse();
        TileDataHelper lastTile;
        bool result = tilemap.GetComponent<EditableTilemap>().tileHelperMap.TryGetValue(currentTileCoordUnderMouse, out lastTile);
        if (result && !lastTile.locked)
        {
            tilemap.SetTile(lastTilePos, null);
            tileHelperMap.Remove(lastTilePos);
        }
    }

    protected void Setup(EditableRuleTile[] tiles)
    {
        this.tiles = tiles;

        tileHelperMap = new Dictionary<Vector3Int, TileDataHelper>();

        grid = GetComponentInParent<Grid>();
        tilemap = GetComponent<Tilemap>();

        selectedTileForPaint = tiles[0];

        TileFlags flags = TileFlags.LockTransform;
        EditableRuleTile edit = (EditableRuleTile)selectedTileForPaint;
        if (edit.IsRotatable)
        {
            flags = TileFlags.None;
        }
        tileHelperMap.Add(new Vector3Int(0,0,0), new TileDataHelper(false, flags));
        tilemap.SetTile(new Vector3Int(0, 0, 0), tiles[0]);

        

        //UITileSprite = new GameObject();
        //SpriteRenderer sp = UITileSprite.AddComponent<SpriteRenderer>();
        //sp.sprite = 
    }
	
	// Update is called once per frame
	protected void Update () {

        if (!InBuildMode)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            TrySetTileUnderMouse(true);
        }
        if (Input.GetMouseButtonDown(1))
        {
            TrySetTileUnderMouse(false);
        }

        bool selectedTileChanged = false;
        if (Input.mouseScrollDelta.y > 0)
        {
            selectedTileIndex = Helper.Mod((selectedTileIndex + 1), tiles.Length);
            selectedTileForPaint = tiles[selectedTileIndex];
            selectedTileChanged = true;
        }
        else if (Input.mouseScrollDelta.y < 0)
        {
            selectedTileIndex = Helper.Mod((selectedTileIndex - 1), tiles.Length);
            selectedTileForPaint = tiles[selectedTileIndex];
            selectedTileChanged = true;
        }

        Vector3Int currentTileCoordUnderMouse = GetTileCoordUnderMouse();
        TileBase tileUnderMouse = tilemap.GetTile(currentTileCoordUnderMouse);

        // If the tile under the mouse is null (unset) then set it to the current selection (for "ghost" placement)
        // and then get the newly painted tile and set its initial values.
        if (tileUnderMouse == null)
        {
            TileFlags flags = TileFlags.LockTransform;
            EditableRuleTile edit = (EditableRuleTile) selectedTileForPaint;
            if (edit.IsRotatable)
            {
                flags = TileFlags.None;
            }
            tileHelperMap.Add(currentTileCoordUnderMouse, new TileDataHelper(false, flags));

            tilemap.SetTile(currentTileCoordUnderMouse, selectedTileForPaint);
        }

        // If the selected paint tile changed (mouse wheel) or the mouse moved into another square
        // get the tile that the mouse just left (should never be null due to mouseover painting code above).
        // If the tile we left wasn't locked in (placed) by the user clicking then set it to null.
        // Update the current tile coordinate the mouse is over to the new position.
        if (selectedTileChanged || lastTilePos != currentTileCoordUnderMouse)
        {
            TileDataHelper lastTile;
            bool result = tilemap.GetComponent<EditableTilemap>().tileHelperMap.TryGetValue(lastTilePos, out lastTile);
            if (result && !lastTile.locked)
            {
                tilemap.SetTile(lastTilePos, null);
                tileHelperMap.Remove(lastTilePos);
            }
        }
        lastTilePos = currentTileCoordUnderMouse;

        
        if (Input.GetKeyDown(KeyCode.R))
        {
            currentAngle = (currentAngle + 90) % 360;

            EditableRuleTile edit = (EditableRuleTile)tilemap.GetTile(currentTileCoordUnderMouse);
            edit.TryRotate(currentTileCoordUnderMouse, tilemap, edit, currentAngle);
            //EditableRuleTile editableTile = (EditableRuleTile) tilemap.GetTile(currentTileCoordUnderMouse);
            //if (tileHelperMap[currentTileCoordUnderMouse].transforms.Count > 0)
            //{
            //    tileHelperMap[currentTileCoordUnderMouse].transformsIndex = (tileHelperMap[currentTileCoordUnderMouse].transformsIndex + 1) % tileHelperMap[currentTileCoordUnderMouse].transforms.Count;
            //    tilemap.SetTile(currentTileCoordUnderMouse, selectedTileForPaint);
            //}
            //Debug.Log(tileHelperMap[currentTileCoordUnderMouse].transformsIndex + "   " + tileHelperMap[currentTileCoordUnderMouse].transforms.Count);
        }

    }

    void TrySetTileUnderMouse(bool placing)
    {
        Vector3Int tileCoord = GetTileCoordUnderMouse();

        if (Mathf.Abs(tileCoord.x) <= editDistanceAroundOrigin.x && Mathf.Abs(tileCoord.y) <= editDistanceAroundOrigin.y)
        {
            //tilemap.SetTile(tileCoord, tile);
            if (placing)
            {
                TileDataHelper lastTile;
                bool result = tilemap.GetComponent<EditableTilemap>().tileHelperMap.TryGetValue(lastTilePos, out lastTile);
                if (result)
                {
                    tileHelperMap[tileCoord].locked = true;
                }
                //placedTileData.Add(new TileDataHelper(tileCoord, 0));
                
                //EditableRuleTile ed = (EditableRuleTile)tilemap.GetTile(tileCoord);
            }
            else
            {
                tileHelperMap.Remove(tileCoord);
                tilemap.SetTile(tileCoord, null);
            }
        }
    }

    Vector3Int GetTileCoordUnderMouse()
    {
        Vector3 worldClickPos = Helper.GetWorldPositionOnPlane(Input.mousePosition, grid.transform.position.z);
        return tilemap.WorldToCell(worldClickPos);
    }

}