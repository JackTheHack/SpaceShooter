﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipGenerator : MonoBehaviour {

    public Ship shipPrefab;
    static ShipGenerationData genData;

    public static int[] PlayerMask = new int[] {
            5,5,0,0,0,3,1,2,
            5,5,0,0,0,3,1,1,
            0,0,0,0,0,3,2,2,
            0,0,0,0,0,3,2,1,
            0,0,0,0,2,2,2,1,
            0,0,0,0,2,2,2,1,
            0,0,0,0,0,1,4,2,
            0,0,0,0,0,1,2,4,
            0,0,0,0,0,1,4,4,
            0,0,0,0,0,1,4,2,
            0,0,0,0,0,3,2,2,
            0,0,0,0,0,3,2,2,
            0,0,0,0,0,0,2,1,
            0,0,2,2,2,4,2,1,
            4,4,4,4,2,2,2,1,
            0,0,0,2,2,2,2,2
    };

    public static Dictionary<ShipSize, int> weaponSlotLimitsByShipSize;
    public static Dictionary<ShipSize, Vector2Int> shipSpriteSizeByShipSize;

    [System.Serializable]
    class ShipGenerationData
    {
        public ShipSizeStringToWeaponSlotLimits weaponSlotLimits;
        public ShipSizeStringToSpriteSize spriteSizes;
    }

    [System.Serializable]
    class ShipSizeStringToWeaponSlotLimits
    {
        public int tiny;
        public int small;
        public int medium;
        public int large;
        public int huge;
    }

    [System.Serializable]
    class ShipSizeStringToSpriteSize
    {
        public Vector2Int tiny;
        public Vector2Int small;
        public Vector2Int medium;
        public Vector2Int large;
        public Vector2Int huge;
    }

    // Use this for initialization
    void Start () {

        string jsonData = Resources.Load<TextAsset>("shipGenerationData").ToString();
        genData = JsonUtility.FromJson<ShipGenerationData>(jsonData);

        weaponSlotLimitsByShipSize = new Dictionary<ShipSize, int>();
        weaponSlotLimitsByShipSize.Add(ShipSize.TINY, genData.weaponSlotLimits.tiny);
        weaponSlotLimitsByShipSize.Add(ShipSize.SMALL, genData.weaponSlotLimits.small);
        weaponSlotLimitsByShipSize.Add(ShipSize.MEDIUM, genData.weaponSlotLimits.medium);
        weaponSlotLimitsByShipSize.Add(ShipSize.LARGE, genData.weaponSlotLimits.large);
        weaponSlotLimitsByShipSize.Add(ShipSize.HUGE, genData.weaponSlotLimits.huge);

        shipSpriteSizeByShipSize = new Dictionary<ShipSize, Vector2Int>();
        shipSpriteSizeByShipSize.Add(ShipSize.TINY, genData.spriteSizes.tiny);
        shipSpriteSizeByShipSize.Add(ShipSize.SMALL, genData.spriteSizes.small);
        shipSpriteSizeByShipSize.Add(ShipSize.MEDIUM, genData.spriteSizes.medium);
        shipSpriteSizeByShipSize.Add(ShipSize.LARGE, genData.spriteSizes.large);
        shipSpriteSizeByShipSize.Add(ShipSize.HUGE, genData.spriteSizes.huge);

    }

    private void Update()
    {
    }

    //public Ship GenerateRandomPlayerShip()
    //{
    //    ShipSize playerShipSize = ShipSize.SMALL;

    //    Ship playerShip = Instantiate(shipPrefab);
    //    Sprite s = GenerateRandomPlayerSprite(playerShipSize);
    //    playerShip.Setup(playerShipSize, s, 4.7f, 2.5f, true);

    //    playerShip.name = "PlayerShip";

    //    return playerShip;
    //}

    public static Sprite GenerateRandomPlayerSprite(ShipSize size)
    {
        //int[] mask = (int[]) PlayerMask.Clone(); // this will currently break with any size other than small because PlayerMask is only small sized.
        int maskWidth = shipSpriteSizeByShipSize[size].x;
        int maskHeight = shipSpriteSizeByShipSize[size].y;

        int[] mask = GenerateShipMask(maskWidth, maskHeight);

        Color shipColor = SpriteGenerator.GetRandomHSVColour(0f, 1f, 0.6f, 1f, 0.5f, 1f);

        Color[] pixels = SpriteGenerator.GetSymmetricalMaskedPixels(maskWidth, maskHeight, mask, shipColor, Color.clear, true);

        pixels = OutlineSprite(pixels, shipColor * 2, maskWidth * 2, maskHeight);

        return SpriteGenerator.GetSpriteFromPixels(maskWidth*2, maskHeight, pixels);
    }

    public static Sprite GenerateWeaponSprite()
    {
        int[] mask = new int[32];
        for (int i = 0; i < mask.Length; i++)
        {
            mask[i] = 2;
        }

        return SpriteGenerator.GenerateMaskSprite(4, 8, mask, Color.white, Color.clear, false);
    }

    public static Color[] OutlineSprite(Color[] pixels, Color outlineColour, int width, int height)
    {
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int current = width * y + x;

                if (pixels[current] != Color.clear)
                {
                    if (x > 0 && pixels[current - 1] == Color.clear)
                    {
                        pixels[current] = outlineColour;
                    }
                    else if (x < width - 1 && pixels[current + 1] == Color.clear)
                    {
                        pixels[current] = outlineColour;
                    }
                    else if (y > 0 && pixels[current - width] == Color.clear)
                    {
                        pixels[current] = outlineColour;
                    }
                    else if (y < height - 1 && pixels[current + width] == Color.clear)
                    {
                        pixels[current] = outlineColour;
                    }
                }
            }
        }

        int outlineWidth = 1;
        for (int i = 0; i < outlineWidth-1; i++)
        {
            Color[] copy = new Color[pixels.Length];
            pixels.CopyTo(copy, 0);

            Color col;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int current = width * y + x;

                    col = pixels[current];

                    if (pixels[current] != outlineColour)
                    {
                        if (x > 0 && pixels[current - 1] == outlineColour)
                        {
                            col = outlineColour;
                        }
                        else if (x < width - 1 && pixels[current + 1] == outlineColour)
                        {
                            col = outlineColour;
                        }
                        else if (y > 0 && pixels[current - width] == outlineColour)
                        {
                            col = outlineColour;
                        }
                        else if (y < height - 1 && pixels[current + width] == outlineColour)
                        {
                            col = outlineColour;
                        }
                    }

                    copy[current] = col;
                }
            }

            copy.CopyTo(pixels, 0);

        }
        
        return pixels;
    }

    // Generate a mask with which to generate sprites.
    // Width should generally be half the intended width
    // since ship sprites are usually created by mirroring the mask.
    public static int[] GenerateShipMask(Vector2Int size)
    {
        return GenerateShipMask(size.x, size.y);
    }

    public static int[] GenerateShipMask(int width, int height)
    {

        float curveFactor = Random.Range(2f, 14f); // lower numbers are blockier.

        System.Func<float, float> trigFunc;

        trigFunc = x => Mathf.Sin(x);

        if (Random.value > 0.333f)
        {
            trigFunc = x => Mathf.Cos(x);
        }
        else if (Random.value > 0.666f)
        {
            trigFunc = x => Mathf.Tan(x);
        }


        int[] mask = new int[width * height];
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

                int choice = 0;
                //if (x > width/2 && x < width-1)
                //{
                //    choice = 2;
                //}
                //if (x <= width/2 && y > height/2)
                //{
                //    choice = 0;
                //}
                //if (x <= Mathf.Abs(Mathf.Sin(width)) * width && y < height/3)
                //{
                //    choice = Random.Range(2,4);
                //}

                //if (x <= Mathf.Cos(Mathf.Sin(y)) * width) Cos(Sin(y)) was accidentally cool looking, like a DNA mouth


                if (x >= Mathf.Abs(trigFunc(( (2*Mathf.PI) / height) * y)) * curveFactor)
                {
                    choice = 2;
                }
                if (y < height * 0.1 && x < width * 0.5 || y > height * 0.9 && x < width * 0.333)
                {
                    choice = 0;
                }

                if (x > width * 0.75)
                {
                    choice = 2;
                }


                mask[width * y + x] = choice;
            }
        }

        return mask;
    }

}