﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour {

    public Ship attachedShip;

    Vector2 CameraExtents;

    Rigidbody2D rb2d;

    float forwardAccel;
    float horizontalAccel;
    float turn;

    Vector2 spriteExtents;

    Vector2 shipToMouse;

    GameObject backgroundQuad;

    private void Start()
    {
        
    }

    public void Setup(Ship newAttached)
    {
        attachedShip = newAttached;
        rb2d = attachedShip.GetComponent<Rigidbody2D>();

        FollowCamera cam = Camera.main.GetComponent<FollowCamera>();
        cam.target = attachedShip.transform;
        cam.targetOffset = newAttached.tilemap.cellSize / 2f;
        //CameraExtents = new Vector2(Camera.main.orthographicSize * Screen.width / Screen.height, Camera.main.orthographicSize);
        spriteExtents = attachedShip.GetComponent<Renderer>().bounds.extents;

        backgroundQuad = GameObject.Find("BackgroundQuad");
    }
	
	// Update is called once per frame
	void Update ()
    {

        if (attachedShip == null)
        {
            return;
        }

        forwardAccel = Input.GetAxisRaw("Vertical");
        horizontalAccel = Input.GetAxisRaw("Horizontal");

        Vector3 mouseScreenPosition = Helper.GetWorldPositionOnPlane(Input.mousePosition, attachedShip.transform.position.z);
        shipToMouse = (mouseScreenPosition - attachedShip.transform.position).normalized;

        Vector3 backgroundPos = backgroundQuad.transform.position;
        Vector3 playerPos = attachedShip.transform.position;
        playerPos.z = backgroundPos.z;

        backgroundQuad.transform.position = playerPos;
        Debug.DrawRay(attachedShip.transform.position, shipToMouse);
    }

    private void FixedUpdate()
    {

        if (attachedShip == null)
        {
            return;
        }

        if (forwardAccel < 0)
        {
            forwardAccel *= 0.5f;
        }

        for (int i = 0; i < attachedShip.shipThrusters.Length; i++)
        {
            Vector2 verticalForce = attachedShip.shipThrusters[i].ApplyAvailableThrust((forwardAccel * Vector2.up).normalized);
            rb2d.AddForceAtPosition(verticalForce, attachedShip.shipThrusters[i].transform.position - (attachedShip.tilemap.cellSize * 0.5f));

            Vector2 horizontalForce = attachedShip.shipThrusters[i].ApplyAvailableThrust((horizontalAccel * Vector2.right).normalized);
            rb2d.AddForceAtPosition(horizontalForce, attachedShip.shipThrusters[i].transform.position - (attachedShip.tilemap.cellSize * 0.5f));
        }

        //Vector3 worldForceAtPosition = attachedShip.transform.TransformPoint(new Vector2(0, -spriteExtents.y));
        //rb2d.AddForceAtPosition(attachedShip.transform.up * forwardAccel * attachedShip.MovementSpeed, worldForceAtPosition);

        //rb2d.AddForce(attachedShip.transform.right * horizontalAccel * attachedShip.MovementSpeed * 0.5f);
    }

    private void LateUpdate()
    {

        if (attachedShip == null)
        {
            return;
        }

        //Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, shipToMouse);
        //var str = Mathf.Min(attachedShip.TurnSpeed * Time.deltaTime, 1);
        rb2d.AddTorque(Vector2.SignedAngle(attachedShip.transform.up, shipToMouse) * Time.fixedDeltaTime * attachedShip.TurnSpeed);

        //attachedShip.transform.rotation = Quaternion.Lerp(attachedShip.transform.rotation, targetRotation, str);
    }
}
