﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipAI : MonoBehaviour {

    Rigidbody2D rb2d;
    Ship attachedShip;
    WeaponMono[] attachedShipWeapons;

    Vector2 spriteExtents;

    PlayerShipManager playerShipManager;
    Ship targetShip;
    

	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        attachedShip = GetComponent<Ship>();

        spriteExtents = GetComponent<SpriteRenderer>().bounds.extents;

        playerShipManager = GameObject.Find("PlayerShipManager").GetComponent<PlayerShipManager>();
        targetShip = playerShipManager.currentlyPiloted;

        attachedShipWeapons = attachedShip.GetComponentsInChildren<WeaponMono>();
    }
	
	// Update is called once per frame
	void Update () {
        targetShip = playerShipManager.currentlyPiloted;

        for (int i = 0; i < attachedShipWeapons.Length; i++)
        {
            attachedShipWeapons[i].ShootButtonHeld();
        }

    }

    private void FixedUpdate()
    {
        Vector3 worldForceAtPosition = attachedShip.transform.TransformPoint(new Vector2(0, -spriteExtents.y));

        

        // For now just move forward and turn towards target.
        rb2d.AddForceAtPosition(attachedShip.transform.up * attachedShip.MovementSpeed, worldForceAtPosition);

        //rb2d.AddForce(attachedShip.transform.right * attachedShip.MovementSpeed * 0.5f);
    }

    private void LateUpdate()
    {
        Vector2 shipToTarget = targetShip.transform.position - attachedShip.transform.position;
        Quaternion targetRotation = Quaternion.LookRotation(Vector3.forward, shipToTarget);
        var str = Mathf.Min(attachedShip.TurnSpeed * Time.deltaTime, 1);
        attachedShip.transform.rotation = Quaternion.Lerp(attachedShip.transform.rotation, targetRotation, str);
    }
}
