﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponControllerType { BULLET, MISSILE, BEAM }

public class Weapon {

    public bool repeatsWhileButtonHeld { get; private set; }

    public int baseDamage { get; private set; }
    public float rateOfFire { get; private set; } // per second
    public float projectileSpeed { get; private set; }
    public ProjectilePattern shootPattern { get; private set; }

    public bool mustBeCharged { get; private set; }

    public float chargeTime { get; private set; }
    public float cooldownTime { get; private set; }

    public bool shootsOnButtonRelease { get; private set; }

    public float lifeTime { get; private set; }

    public Sprite projectileSprite { get; private set; }

    //GameObject projectilePrefab;

    public WeaponControllerType controlType;

    public Weapon(int baseDamage, float cooldownTime, float chargeTime, float projectileSpeed, float lifeTime, ProjectilePattern shootPattern, bool repeatsWhileButtonHeld, bool shootsOnButtonRelease, WeaponControllerType controlType)
    {
        this.repeatsWhileButtonHeld = repeatsWhileButtonHeld;
        this.baseDamage = baseDamage;
        this.rateOfFire = 1f / (cooldownTime + chargeTime);
        this.projectileSpeed = projectileSpeed;
        this.lifeTime = lifeTime;
        this.shootPattern = shootPattern;
        this.mustBeCharged = chargeTime > 0f;

        this.chargeTime = chargeTime;
        this.cooldownTime = cooldownTime;

        this.shootsOnButtonRelease = shootsOnButtonRelease;
        this.controlType = controlType;

        //this.stateManager = new WeaponStateManager(chargeTime, cooldownTime);

        //this.projectilePrefab = projectilePrefab;

        // Set up graphical projectile of weapon (dynamically created based on weapon stats).
        this.projectileSprite = CreateRoundBulletSprite();
    }

    public static T[] FlattenArray<T>(T[][] arr)
    {
        if (arr.Length == 0)
        {
            return new T[0];
        }

        int innerLength = arr[0].Length;
        T[] flat = new T[arr.Length * innerLength];
        for (int i = 0; i < arr.Length; i++)
        {
            for (int j = 0; j < innerLength; j++)
            {
                flat[(i * innerLength) + j] = arr[i][j];
            }
        }
        return flat;
    }

    private Sprite CreateRoundBulletSprite()
    {
        int pixelsWidthHeight = Mathf.RoundToInt(baseDamage);

        if (pixelsWidthHeight == 0)
        {
            pixelsWidthHeight++;
        }

        Color[][] workingPixels = new Color[pixelsWidthHeight][];
        for (int i = 0; i < workingPixels.Length; i++)
        {
            workingPixels[i] = new Color[pixelsWidthHeight];
        }

        float hue = Random.value;
        float sat = Random.Range(0.5f, 1f);
        float val = Random.Range(0.4f, 0.6f);

        Color initialColor = Color.HSVToRGB(hue, sat, val);

        //float endCapPointyNess = 1 / projectileSpeed; // Larger is LESS pointy.

        float halfWidth = pixelsWidthHeight / 2f;

        //Generate bullet round
        for (int y = 0; y < pixelsWidthHeight; y++)
        {
            for (int x = 0; x < pixelsWidthHeight; x++)
            {
                float yShift = y - halfWidth;

                float alpha = 0f;
                if ( x - pixelsWidthHeight > -Mathf.Sqrt((halfWidth * halfWidth) - (yShift*yShift)))
                {
                    alpha = 1;
                }
                workingPixels[y][x] = initialColor;//Color.Lerp(initialColor, Color.white, j / ((float)workingPixels[i].Length));
                workingPixels[y][x].a = alpha;
            }
        }

        Color[] finalPixels = FlattenArray<Color>(workingPixels);
        // Anti-alias
        for (int i = 0; i < finalPixels.Length; i++)
        {
            if (finalPixels[i].a == 0 && i % pixelsWidthHeight != 0)
            {
                if (i - 1 > 0 && finalPixels[i - 1].a > 0)
                {
                    finalPixels[i - 1].a = 0.5f;
                }
                if (i + 1 < finalPixels.Length - 1 && finalPixels[i + 1].a > 0)
                {
                    finalPixels[i + 1].a = 0.5f;
                }
            }
        }
        //return SpriteGenerator.GetSpriteFromPixels(pixelsWidth, pixelsHeight, finalPixels);
        return SpriteGenerator.GetMirroredSprite(pixelsWidthHeight, pixelsWidthHeight, finalPixels);
    }

    private Sprite CreateBulletSprite()
    {
        int pixelsWidth =  Mathf.RoundToInt(baseDamage * 0.5f) + Random.Range(-1, 1);
        int pixelsHeight = Mathf.RoundToInt(baseDamage * 0.5f * 4f) + Random.Range(-1, 1);

        if (pixelsWidth == 0)
        {
            pixelsWidth++;
        }
        if (pixelsHeight == 0)
        {
            pixelsHeight++;
        }

        Color[][] workingPixels = new Color[pixelsHeight][];
        for (int i = 0; i < workingPixels.Length; i++)
        {
            workingPixels[i] = new Color[pixelsWidth];
        }

        float hue = Random.value;
        float sat = Random.Range(0.5f, 1f);
        float val = Random.Range(0.4f, 0.6f);

        Color initialColor = Color.HSVToRGB(hue, sat, val);

        float endCapPointyNess = 1 / projectileSpeed; // Larger is LESS pointy.

        //Generate bullet cap
        for (int i = 0; i < pixelsHeight; i++)
        {
            for (int j = 0; j < workingPixels[i].Length; j++)
            {
                float alpha = 0f;
                if ( ((i+1)*endCapPointyNess) + j > Mathf.CeilToInt(workingPixels[i].Length * 0.5f))
                {
                    alpha = 1;
                }
                workingPixels[i][j] = Color.Lerp(initialColor, Color.white, j / ((float)workingPixels[i].Length));
                workingPixels[i][j].a = alpha;
            }
        }

        Color[] finalPixels = FlattenArray<Color>(workingPixels);
        // Anti-alias
        for (int i = 0; i < finalPixels.Length; i++)
        {
            if (finalPixels[i].a == 0 && i % pixelsWidth != 0)
            {
                if (i - 1 > 0 && finalPixels[i - 1].a > 0)
                {
                    finalPixels[i - 1].a = 0.5f;
                }
                if (i + 1 < finalPixels.Length - 1 && finalPixels[i + 1].a > 0)
                {
                    finalPixels[i + 1].a = 0.5f;
                }
            }
        }
        //return SpriteGenerator.GetSpriteFromPixels(pixelsWidth, pixelsHeight, finalPixels);
        return SpriteGenerator.GetMirroredSprite(pixelsWidth, pixelsHeight, finalPixels);
    }
}

/*
 * Defines how projectiles shoot from a weapon, such as a spread or a burst.
 */
public class ProjectilePattern
{
    public int amount;
    public float spreadAngle;
    public float delayBetweenProjectiles;

    public ProjectilePattern(int amount, float spreadAngle, float delayBetweenProjectiles)
    {
        this.amount = amount;
        this.spreadAngle = spreadAngle;
        this.delayBetweenProjectiles = delayBetweenProjectiles;
    }

    // Attempt to describe the characteristics of the shoot pattern.
    string GetApproximateDescription()
    {
        string description = "";
        if (amount > 1 && delayBetweenProjectiles > 0 && spreadAngle < 20)
        {
            description += "Burst ";
        }
        if (amount == 1 && spreadAngle > 5)
        {
            description += "Inaccurate ";
        }
        if (amount > 1 && delayBetweenProjectiles == 0 && spreadAngle > 30)
        {
            description += "Spread ";
        }
        if (amount == 1 && delayBetweenProjectiles == 0)
        {
            description += "Single ";
        }
        
        if (description == string.Empty)
        {
            description = "Inconclusive";
        }

        return description;
    }
}